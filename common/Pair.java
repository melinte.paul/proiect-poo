package common;

import java.util.Objects;

/*
 * Clasa ce defineste tipul Pair in mod generic
 */
public final class Pair<E, T> {
    private E key;
    private T value;

    public Pair(final E key, final T value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(key, pair.key) && Objects.equals(value, pair.value);
    }

    public E getKey() {
        return key;
    }

    public T getValue() {
        return value;
    }
}
