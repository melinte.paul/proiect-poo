package common.constants;

/*
 * Constante generale
 */
public final class Constants {
    private Constants() {
    }

    //Xp primit kill-uri
    public static final int BASE_REC_XP = 200;
    public static final int ADDED_REC_XP = 40;

    //XP necesar level-up
    public static final int BASE_XP = 250;
    public static final int ADDED_XP = 50;

}
