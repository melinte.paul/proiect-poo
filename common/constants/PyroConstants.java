package common.constants;

/*
 * Constante specifice eroului Pyromancer
 */
public final class PyroConstants {
    public static final int BASE_HP = 500;
    public static final int ADDED_HP = 50;

    public static final float TERRAIN_MOD = 1.25f;

    private PyroConstants() {
    }

    public static final class FirstAbilityMods {
        public static final float KNIGHT = 1.2f;
        public static final float PYRO = 0.9f;
        public static final float ROGUE = 0.8f;
        public static final float WIZARD = 1.05f;
    }

    public static final class SecondAbilityMods {
        public static final float KNIGHT = 1.2f;
        public static final float PYRO = 0.9f;
        public static final float ROGUE = 0.8f;
        public static final float WIZARD = 1.05f;
    }

    public static final int FIRST_ABILITY_DMG = 350;
    public static final int FIRST_ABILITY_PROG = 50;

    public static final int SECOND_ABILITY_DMG = 150;
    public static final int SECOND_ABILITY_PROGRESS = 20;

    public static final int BURN_DMG = 50;
    public static final int BURN_PROGRESS = 30;
}
