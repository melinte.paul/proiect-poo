package common.constants;

/*
 * Constante specifice eroului Wizard
 */
public final class WizardConstants {
    public static final int BASE_HP = 400;
    public static final int ADDED_HP = 30;

    public static final float TERRAIN_MOD = 1.1f;

    private WizardConstants() {
    }

    public static final class FirstAbilityMods {
        public static final float KNIGHT = 1.2f;
        public static final float PYRO = 0.9f;
        public static final float ROGUE = 0.8f;
        public static final float WIZARD = 1.05f;
    }

    public static final class SecondAbilityMods {
        public static final float KNIGHT = 1.4f;
        public static final float PYRO = 1.3f;
        public static final float ROGUE = 1.2f;
        public static final float WIZARD = 0f;
    }

    public static final float FRST_ABIL_DMG = 0.2f;
    public static final float FRST_ABIL_PROG = 0.05f;
    public static final float FRST_ABIL_THRESHOLD = 0.3f;

    public static final float SCND_ABIL_DMG = 0.35f;
    public static final float SCND_ABIL_PROGRESS = 0.02f;
    public static final float SCND_ABIL_THRESHOLD = 0.7f;

}
