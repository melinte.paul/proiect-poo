package common.constants;

/*
 * Constante specifice eroului Rogue
 */
public final class RogueConstants {
    public static final int BASE_HP = 600;
    public static final int ADDED_HP = 40;

    public static final float TERRAIN_MOD = 1.15f;

    private RogueConstants() {
    }

    public static final class FirstAbilityMods {
        public static final float KNIGHT = 0.9f;
        public static final float PYRO = 1.25f;
        public static final float ROGUE = 1.2f;
        public static final float WIZARD = 1.25f;
    }

    public static final class SecondAbilityMods {
        public static final float KNIGHT = 0.8f;
        public static final float PYRO = 1.2f;
        public static final float ROGUE = 0.9f;
        public static final float WIZARD = 1.25f;
    }

    public static final int FRST_ABIL_DMG = 200;
    public static final int FRST_ABIL_PROG = 20;

    public static final int SCND_ABIL_DMG = 40;
    public static final int SCND_ABIL_PROGRESS = 10;

    public static final int BS_TURNS = 3;
    public static final float BS_FOREST_MOD = 1.5f;

    public static final int PARA_DURATION = 3;
}
