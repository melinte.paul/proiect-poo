package common.constants;

/*
 * Constante specifice eroului Knight
 */
public final class KnightConstants {
    private KnightConstants() {
    }

    public static final int BASE_HP = 900;
    public static final int ADDED_HP = 80;

    public static final float TERRAIN_MOD = 1.15f;

    public static final class FirstAbilityMods {
        public static final float KNIGHT = 1f;
        public static final float PYRO = 1.1f;
        public static final float ROGUE = 1.15f;
        public static final float WIZARD = 0.8f;
    }

    public static final class SecondAbilityMods {
        public static final float KNIGHT = 1.2f;
        public static final float PYRO = 0.9f;
        public static final float ROGUE = 0.8f;
        public static final float WIZARD = 1.05f;
    }

    public static final float EXECUTE_ABIL_DMG = 0.2f;
    public static final float EXECUTE_ABIL_PROGRESS = 0.01f;
    public static final float EXECUTE_ABIL_THRESHOLD = 0.4f;

    public static final int FIRST_ABILITY_DMG = 200;
    public static final int FIRST_ABILITY_PROG = 30;

    public static final int SECOND_ABILITY_DMG = 100;
    public static final int SECOND_ABILITY_PROGRESS = 40;
}
