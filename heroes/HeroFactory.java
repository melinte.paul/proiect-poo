package heroes;

/*
 * Clasa ce se ocupa de generarea de eroi
 */
public final class HeroFactory {
    private static HeroFactory instance = null;

    private HeroFactory() {

    }

    public static HeroFactory getInstance() {
        if (instance == null) {
            instance = new HeroFactory();
        }

        return instance;
    }

    public Hero getHero(final char c, final int x, final int y) {
        switch (c) {
            case 'K':
                return new Knight(x, y);
            case 'P':
                return new Pyromancer(x, y);
            case 'R':
                return new Rogue(x, y);
            case 'W':
                return new Wizard(x, y);
            default:
                System.out.println("Error hero maker");
                return null;
        }
    }

}
