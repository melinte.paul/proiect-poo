package heroes;

import common.constants.WizardConstants;
import heroes.effects.TurnEffect;
import map.AreaTypes;

import static common.constants.WizardConstants.ADDED_HP;
import static common.constants.WizardConstants.BASE_HP;
import static common.constants.WizardConstants.FRST_ABIL_DMG;
import static common.constants.WizardConstants.FRST_ABIL_PROG;
import static common.constants.WizardConstants.FRST_ABIL_THRESHOLD;
import static common.constants.WizardConstants.SCND_ABIL_DMG;
import static common.constants.WizardConstants.SCND_ABIL_PROGRESS;
import static common.constants.WizardConstants.SCND_ABIL_THRESHOLD;
import static common.constants.WizardConstants.TERRAIN_MOD;
import static java.lang.Math.min;
import static java.lang.Math.round;

/*
 * Clasa ce descrie eroul Wizard
 */

public final class Wizard extends Hero {
    Wizard(final int x, final int y) {
        super(x, y);
        setType('W');

        setPriority(1);
        setCurrentHP(BASE_HP);
        setMaxHP(BASE_HP);
    }

    @Override
    void levelUP() {
        setMaxHP(getMaxHP() + ADDED_HP);
        setCurrentHP(getMaxHP());
    }

    @Override
    public void acceptFight(final Hero attacker, final AreaTypes areaType) {
        setTurnEffect(attacker.fight(this, areaType));

    }

    private TurnEffect getEffect(final AreaTypes areaType, final float mod1, final float mod2,
                                 final boolean deflect, final Hero attacked) {
        float areaMod = getAreaMod(areaType);

        float drainPercent = mod1 * (FRST_ABIL_DMG + FRST_ABIL_PROG * getLevel());
        int drainDamage = round(drainPercent * min(FRST_ABIL_THRESHOLD * attacked.getMaxHP(),
                attacked.getCurrentHP()) * areaMod);

        int deflectDamage = 0;
        if (deflect) {
            deflectDamage = round(getTurnEffect().getBaseDamage()
                    * (min(SCND_ABIL_THRESHOLD, SCND_ABIL_DMG + SCND_ABIL_PROGRESS * getLevel()))
                    * areaMod * mod2);
        }

        return new TurnEffect(this, drainDamage + deflectDamage,
                drainDamage + deflectDamage, null);

    }

    private float getAreaMod(final AreaTypes areaType) {
        if (areaType == AreaTypes.desert) {
            return TERRAIN_MOD;
        }
        return 1f;
    }


    @Override
    public TurnEffect fight(final Knight attacked, final AreaTypes areaType) {
        return getEffect(areaType, WizardConstants.FirstAbilityMods.KNIGHT,
                WizardConstants.SecondAbilityMods.KNIGHT, true, attacked);
    }

    @Override
    public TurnEffect fight(final Pyromancer attacked, final AreaTypes areaType) {
        return getEffect(areaType, WizardConstants.FirstAbilityMods.PYRO,
                WizardConstants.SecondAbilityMods.PYRO, true, attacked);
    }

    @Override
    public TurnEffect fight(final Rogue attacked, final AreaTypes areaType) {
        return getEffect(areaType, WizardConstants.FirstAbilityMods.ROGUE,
                WizardConstants.SecondAbilityMods.ROGUE, true, attacked);
    }

    /*
     * In cazul in care se lupta cu un Wizard, nu folosim abilitatea deflect
     */
    @Override
    public TurnEffect fight(final Wizard attacked, final AreaTypes areaType) {
        return getEffect(areaType, WizardConstants.FirstAbilityMods.WIZARD,
                WizardConstants.SecondAbilityMods.WIZARD, false, attacked);
    }

}
