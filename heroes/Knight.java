package heroes;

import common.constants.KnightConstants;
import heroes.effects.OverTimeEffect;
import heroes.effects.TurnEffect;
import map.AreaTypes;

import static common.constants.KnightConstants.ADDED_HP;
import static common.constants.KnightConstants.BASE_HP;
import static common.constants.KnightConstants.EXECUTE_ABIL_DMG;
import static common.constants.KnightConstants.EXECUTE_ABIL_PROGRESS;
import static common.constants.KnightConstants.EXECUTE_ABIL_THRESHOLD;
import static common.constants.KnightConstants.FIRST_ABILITY_DMG;
import static common.constants.KnightConstants.FIRST_ABILITY_PROG;
import static common.constants.KnightConstants.SECOND_ABILITY_DMG;
import static common.constants.KnightConstants.SECOND_ABILITY_PROGRESS;
import static java.lang.Math.min;
import static java.lang.Math.round;

/*
 * Clasa ce descrie eroul knight
 */
public final class Knight extends Hero {
    Knight(final int x, final int y) {
        super(x, y);
        setType('K');
        setCurrentHP(BASE_HP);
        setMaxHP(BASE_HP);
    }

    @Override
    void levelUP() {
        setMaxHP(getMaxHP() + ADDED_HP);
        setCurrentHP(getMaxHP());
    }

    private TurnEffect getEffect(final AreaTypes areaType, final float mod1, final float mod2) {
        float areaMod = getAreaMod(areaType);

        int baseDMG = (round((FIRST_ABILITY_DMG + (FIRST_ABILITY_PROG
                * getLevel())) * areaMod)
                + round((SECOND_ABILITY_DMG + (SECOND_ABILITY_PROGRESS
                * getLevel())) * areaMod));

        int finalDMG = round((FIRST_ABILITY_DMG + (FIRST_ABILITY_PROG
                * getLevel())) * areaMod * mod1)
                + round((SECOND_ABILITY_DMG + (SECOND_ABILITY_PROGRESS
                * getLevel())) * areaMod * mod2);

        OverTimeEffect overTimeEffect = new OverTimeEffect(1, 0, 0);

        return new TurnEffect(this, finalDMG, baseDMG, overTimeEffect);
    }

    private float getAreaMod(final AreaTypes areaType) {
        if (areaType == AreaTypes.land) {
            return KnightConstants.TERRAIN_MOD;
        }
        return 1f;
    }

    /*
     * Calculam daca inamicul este executat
     */
    private boolean executeLimit(final Hero attacked) {
        return attacked.getCurrentHP() < round(attacked.getMaxHP()
                * min((EXECUTE_ABIL_DMG + (EXECUTE_ABIL_PROGRESS * getLevel())),
                EXECUTE_ABIL_THRESHOLD));
    }

    @Override
    public void acceptFight(final Hero attacker, final AreaTypes areaType) {
        setTurnEffect(attacker.fight(this, areaType));
    }

    @Override
    public TurnEffect fight(final Knight attacked, final AreaTypes areaType) {
        if (executeLimit(attacked)) {
            return new TurnEffect(this, attacked.getCurrentHP(), attacked.getCurrentHP(), null);
        }

        return getEffect(areaType, KnightConstants.FirstAbilityMods.KNIGHT,
                KnightConstants.SecondAbilityMods.KNIGHT);
    }

    @Override
    public TurnEffect fight(final Pyromancer attacked, final AreaTypes areaType) {
        if (executeLimit(attacked)) {
            return new TurnEffect(this, attacked.getCurrentHP(), attacked.getCurrentHP(), null);
        }

        return getEffect(areaType, KnightConstants.FirstAbilityMods.PYRO,
                KnightConstants.SecondAbilityMods.PYRO);
    }

    @Override
    public TurnEffect fight(final Rogue attacked, final AreaTypes areaType) {
        if (executeLimit(attacked)) {
            return new TurnEffect(this, attacked.getCurrentHP(), attacked.getCurrentHP(), null);
        }

        return getEffect(areaType, KnightConstants.FirstAbilityMods.ROGUE,
                KnightConstants.SecondAbilityMods.ROGUE);
    }

    @Override
    public TurnEffect fight(final Wizard attacked, final AreaTypes areaType) {
        if (executeLimit(attacked)) {
            return new TurnEffect(this, attacked.getCurrentHP(), attacked.getCurrentHP(), null);
        }

        return getEffect(areaType, KnightConstants.FirstAbilityMods.WIZARD,
                KnightConstants.SecondAbilityMods.WIZARD);
    }


}
