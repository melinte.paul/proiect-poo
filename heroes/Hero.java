package heroes;

import common.constants.Constants;
import common.Pair;
import heroes.effects.OverTimeEffect;
import heroes.effects.TurnEffect;
import map.AreaTypes;

import static java.lang.Math.max;

/*
 * Clasa de baza a tuturor eroilor
 */
public abstract class Hero {
    private char type;
    private int x;
    private int y;
    private int priority;
    private int level;
    private int xp;
    private int currentHP;
    private int maxHP;
    private TurnEffect turnEffect;
    private OverTimeEffect overTimeEffect;

    Hero(final int x, final int y) {
        this.setX(x);
        this.setY(y);
        this.setPriority(0);
        setLevel(0);
        setXp(0);
    }

    /*
    * Functia toString respecta scrierea in enunt ceruta de enunt
     */
    @Override
    public final String toString() {
        if (isDead()) {
            return getType() + " dead";
        }
        return getType() + " " + getLevel() + " " + getXp() + " "
                + getCurrentHP() + " " + getX() + " " + getY();
    }

    public final void movement(final char move) {
        if (getOverTimeEffect() == null || !getOverTimeEffect().isParalyzed()) {
            switch (move) {
                case 'U':
                    setX(getX() - 1);
                    break;
                case 'D':
                    setX(getX() + 1);
                    break;
                case 'L':
                    setY(getY() - 1);
                    break;
                case 'R':
                    setY(getY() + 1);
                    break;
                case '_':
                    break;
                default:
                    System.out.println("Eroare miscare");
            }
        }
    }

    public final Pair<Integer, Integer> getPos() {
        return new Pair<>(getX(), getY());
    }

    public final int getPriority() {
        return priority;
    }

    public final void calculatePreTurn() {
        if (getOverTimeEffect() != null) {
            setCurrentHP(getCurrentHP() - getOverTimeEffect().getDamageOverTime());
        }
    }


    public final void calculateEndTurn() {
        if (getTurnEffect() != null && getTurnEffect().getOverTimeEffect() != null) {
            setOverTimeEffect(getTurnEffect().getOverTimeEffect());
        }

        if (getTurnEffect() != null) {
            setCurrentHP(getCurrentHP() - getTurnEffect().getDamage());
        }
    }

    public final void calculateXP() {
        if (isDead() && getTurnEffect() != null) {
            getTurnEffect().getSource().receiveXP(getLevel());
        }

        setTurnEffect(null);
    }

    private void receiveXP(final int enemyLevel) {
        setXp(getXp() + max(0, Constants.BASE_REC_XP
                - (getLevel() - enemyLevel) * Constants.ADDED_REC_XP));
        while (!isDead() && (Constants.BASE_XP + getLevel() * Constants.ADDED_XP) <= getXp()) {
            setLevel(getLevel() + 1);
            levelUP();
        }
    }

    public final boolean isDead() {
        return getCurrentHP() <= 0;
    }

    abstract void levelUP();

    /*
    * Functiile ce se ocupa de double-dispatch
    * Eroul atacat accepta lupa cu eroul atacator
    * Eroul ce ataca trebuie sa aiba functii pentru toate tipurile de eroi
     */
    public abstract void acceptFight(Hero attacker, AreaTypes areaType);

    public abstract TurnEffect fight(Knight attacked, AreaTypes areaType);

    public abstract TurnEffect fight(Pyromancer attacked, AreaTypes areaType);

    public abstract TurnEffect fight(Rogue attacked, AreaTypes areaType);

    public abstract TurnEffect fight(Wizard attacked, AreaTypes areaType);

    private char getType() {
        return type;
    }

    final void setType(final char type) {
        this.type = type;
    }

    private int getX() {
        return x;
    }

    private void setX(final int x) {
        this.x = x;
    }

    private int getY() {
        return y;
    }

    private void setY(final int y) {
        this.y = y;
    }

    final void setPriority(final int priority) {
        this.priority = priority;
    }

    final int getLevel() {
        return level;
    }

    private void setLevel(final int level) {
        this.level = level;
    }

    private int getXp() {
        return xp;
    }

    private void setXp(final int xp) {
        this.xp = xp;
    }

    final int getCurrentHP() {
        return currentHP;
    }

    final void setCurrentHP(final int currentHP) {
        this.currentHP = currentHP;
    }

    final int getMaxHP() {
        return maxHP;
    }

    final void setMaxHP(final int maxHP) {
        this.maxHP = maxHP;
    }

    final TurnEffect getTurnEffect() {
        return turnEffect;
    }

    final void setTurnEffect(final TurnEffect turnEffect) {
        this.turnEffect = turnEffect;
    }

    private OverTimeEffect getOverTimeEffect() {
        return overTimeEffect;
    }

    private void setOverTimeEffect(final OverTimeEffect overTimeEffect) {
        this.overTimeEffect = overTimeEffect;
    }
}
