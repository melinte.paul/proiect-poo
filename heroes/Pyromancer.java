package heroes;

import common.constants.PyroConstants;
import heroes.effects.OverTimeEffect;
import heroes.effects.TurnEffect;
import map.AreaTypes;

import static common.constants.PyroConstants.ADDED_HP;
import static common.constants.PyroConstants.BASE_HP;
import static common.constants.PyroConstants.BURN_DMG;
import static common.constants.PyroConstants.BURN_PROGRESS;
import static common.constants.PyroConstants.FIRST_ABILITY_DMG;
import static common.constants.PyroConstants.FIRST_ABILITY_PROG;
import static common.constants.PyroConstants.SECOND_ABILITY_DMG;
import static common.constants.PyroConstants.SECOND_ABILITY_PROGRESS;
import static common.constants.PyroConstants.TERRAIN_MOD;
import static java.lang.Math.round;

/*
 * Clasa ce descrie eroul Pyromancer
 */
public final class Pyromancer extends Hero {
    Pyromancer(final int x, final int y) {
        super(x, y);
        setType('P');

        setCurrentHP(BASE_HP);
        setMaxHP(BASE_HP);
    }

    @Override
    void levelUP() {
        setMaxHP(getMaxHP() + ADDED_HP);
        setCurrentHP(getMaxHP());
    }

    @Override
    public void acceptFight(final Hero attacker, final AreaTypes areaType) {
        setTurnEffect(attacker.fight(this, areaType));
    }

    private TurnEffect getEffect(final AreaTypes areaType, final float mod1, final float mod2) {
        float areaMod = getAreaMod(areaType);

        int baseDamage = (round((FIRST_ABILITY_DMG + (FIRST_ABILITY_PROG
                * getLevel())) * areaMod)
                + round((SECOND_ABILITY_DMG + (SECOND_ABILITY_PROGRESS
                * getLevel())) * areaMod));

        int finalDMG = round((FIRST_ABILITY_DMG + (FIRST_ABILITY_PROG
                * getLevel())) * areaMod * mod1)
                + round((SECOND_ABILITY_DMG + (SECOND_ABILITY_PROGRESS
                * getLevel())) * areaMod * mod2);


        OverTimeEffect overTimeEffect = new OverTimeEffect(0,
                round(round((BURN_DMG + BURN_PROGRESS * getLevel()) * areaMod) * mod2), 2);

        return new TurnEffect(this, finalDMG, baseDamage, overTimeEffect);
    }

    private float getAreaMod(final AreaTypes areaType) {
        if (areaType == AreaTypes.volcanic) {
            return TERRAIN_MOD;
        }
        return 1f;
    }


    @Override
    public TurnEffect fight(final Knight attacked, final AreaTypes areaType) {
        return getEffect(areaType, PyroConstants.FirstAbilityMods.KNIGHT,
                PyroConstants.SecondAbilityMods.KNIGHT);
    }

    @Override
    public TurnEffect fight(final Pyromancer attacked, final AreaTypes areaType) {
        return getEffect(areaType, PyroConstants.FirstAbilityMods.PYRO,
                PyroConstants.SecondAbilityMods.PYRO);
    }

    @Override
    public TurnEffect fight(final Rogue attacked, final AreaTypes areaType) {
        return getEffect(areaType, PyroConstants.FirstAbilityMods.ROGUE,
                PyroConstants.SecondAbilityMods.ROGUE);
    }

    @Override
    public TurnEffect fight(final Wizard attacked, final AreaTypes areaType) {
        return getEffect(areaType, PyroConstants.FirstAbilityMods.WIZARD,
                PyroConstants.SecondAbilityMods.WIZARD);
    }


}
