package heroes;

import common.constants.RogueConstants;
import heroes.effects.OverTimeEffect;
import heroes.effects.TurnEffect;
import map.AreaTypes;

import static common.constants.RogueConstants.ADDED_HP;
import static common.constants.RogueConstants.BASE_HP;
import static common.constants.RogueConstants.BS_FOREST_MOD;
import static common.constants.RogueConstants.BS_TURNS;
import static common.constants.RogueConstants.FRST_ABIL_DMG;
import static common.constants.RogueConstants.FRST_ABIL_PROG;
import static common.constants.RogueConstants.PARA_DURATION;
import static common.constants.RogueConstants.SCND_ABIL_DMG;
import static common.constants.RogueConstants.SCND_ABIL_PROGRESS;
import static common.constants.RogueConstants.TERRAIN_MOD;
import static java.lang.Math.round;

/*
 * Clasa ce descrie eroul Rogue
 */

public final class Rogue extends Hero {
    private int backstabTurns;

    Rogue(final int x, final int y) {
        super(x, y);
        setType('R');
        backstabTurns = 0;

        setCurrentHP(BASE_HP);
        setMaxHP(BASE_HP);
    }

    @Override
    void levelUP() {
        setMaxHP(getMaxHP() + ADDED_HP);
        setCurrentHP(getMaxHP());
    }

    @Override
    public void acceptFight(final Hero attacker, final AreaTypes areaType) {
        setTurnEffect(attacker.fight(this, areaType));

    }

    private TurnEffect getEffect(final AreaTypes areaType, final float mod1, final float mod2) {
        float areaMod = getAreaMod(areaType);

        float forestMod = 1;
        if (backstabTurns == 0) {
            backstabTurns = BS_TURNS;
            if (areaType.equals(AreaTypes.woods)) {
                forestMod = BS_FOREST_MOD;
            }
        }

        int bsDMG = round((FRST_ABIL_DMG + (FRST_ABIL_PROG * getLevel())) * areaMod * forestMod);
        int paraDamage = round((SCND_ABIL_DMG + (SCND_ABIL_PROGRESS * getLevel())) * areaMod);

        int bsFinalDamage = round((FRST_ABIL_DMG + (FRST_ABIL_PROG * getLevel()))
                * areaMod * forestMod * mod1);
        int paraFinalDamage = round((SCND_ABIL_DMG + (SCND_ABIL_PROGRESS * getLevel()))
                * areaMod * mod2);

        int duration = PARA_DURATION;
        if (areaType.equals(AreaTypes.woods)) {
            duration = 2 * PARA_DURATION;
        }

        OverTimeEffect overTimeEffect = new OverTimeEffect(duration,
                round(round((SCND_ABIL_DMG + SCND_ABIL_PROGRESS * getLevel())
                        * areaMod) * mod2), duration);

        return new TurnEffect(this, bsFinalDamage
                + paraFinalDamage, bsDMG + paraDamage, overTimeEffect);
    }

    private float getAreaMod(final AreaTypes areaType) {
        if (areaType == AreaTypes.woods) {
            return TERRAIN_MOD;
        }
        return 1f;
    }

    @Override
    public TurnEffect fight(final Knight attacked, final AreaTypes areaType) {
        return getEffect(areaType, RogueConstants.FirstAbilityMods.KNIGHT,
                RogueConstants.SecondAbilityMods.KNIGHT);
    }

    @Override
    public TurnEffect fight(final Pyromancer attacked, final AreaTypes areaType) {
        return getEffect(areaType, RogueConstants.FirstAbilityMods.PYRO,
                RogueConstants.SecondAbilityMods.PYRO);
    }

    @Override
    public TurnEffect fight(final Rogue attacked, final AreaTypes areaType) {
        return getEffect(areaType, RogueConstants.FirstAbilityMods.ROGUE,
                RogueConstants.SecondAbilityMods.ROGUE);
    }

    @Override
    public TurnEffect fight(final Wizard attacked, final AreaTypes areaType) {
        return getEffect(areaType, RogueConstants.FirstAbilityMods.WIZARD,
                RogueConstants.SecondAbilityMods.WIZARD);
    }


}
