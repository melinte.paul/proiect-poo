package heroes.effects;

import heroes.Hero;

/*
 * Clasa ce descrie cum a decurs ultima lupta a eroului
 */
public final class TurnEffect {
    private Hero source;
    private int damage;
    private int baseDamage;
    private OverTimeEffect overTimeEffect;

    public TurnEffect(final Hero source, final int damage,
                      final int baseDamage, final OverTimeEffect overTimeEffect) {
        this.source = source;
        this.damage = damage;
        this.baseDamage = baseDamage;
        this.overTimeEffect = overTimeEffect;
    }

    public OverTimeEffect getOverTimeEffect() {
        return overTimeEffect;
    }

    public int getDamage() {
        return damage;
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public Hero getSource() {
        return source;
    }
}
