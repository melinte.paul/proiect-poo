package heroes.effects;

/*
 * Clasa ce descrie efectele pe mai multe runde
 */
public final class OverTimeEffect {
    private int paralysisDuration;
    private int damageOverTime;
    private int dotDuration;

    public OverTimeEffect(final int paralysisDuration,
                          final int damageOverTime, final int dotDuration) {
        this.paralysisDuration = paralysisDuration;
        this.damageOverTime = damageOverTime;
        this.dotDuration = dotDuration;
    }

    public boolean isParalyzed() {
        if (paralysisDuration > 0) {
            paralysisDuration--;
            return true;
        }

        return false;
    }

    public int getDamageOverTime() {
        if (dotDuration > 0) {
            dotDuration--;
            return damageOverTime;
        }
        return 0;
    }

}
