package map;


import common.Pair;

/*
 * Clasa ce se ocupa de gestionarea hartii de joc
 */
public final class GameMap {
    private AreaTypes[][] map;

    public GameMap(final int n, final int m) {
        map = new AreaTypes[n][m];
    }

    public void changeAreaType(final AreaTypes newType, final int x, final int y) {
        map[x][y] = newType;
    }

    public AreaTypes getAreaType(final Pair<Integer, Integer> pos) {
        return map[pos.getKey()][pos.getValue()];
    }
}
