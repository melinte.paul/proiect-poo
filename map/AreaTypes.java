package map;

/*
 * Tipurile de teren
 */
public enum AreaTypes {
    land, volcanic, desert, woods
}
