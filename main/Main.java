package main;

public final class Main {
    private Main() {
    }

    public static void main(final String[] args) {
        IOHandler ioHandler = new IOHandler(args[0], args[1]);
        Game game = new Game(ioHandler);
        game.run();
    }
}
