package main;

import fileio.FileSystem;
import heroes.Hero;
import heroes.HeroFactory;
import map.AreaTypes;
import map.GameMap;

import java.io.IOException;
import java.util.ArrayList;

/*
 * Clasa ce se ocupa cu gestionarea citirii si scrierii din fisier
 */
final class IOHandler {
    private FileSystem fs;

    /*
     * La initializare se deschide un nou FileSystem, acesta trebuie inchis la final
     */
    IOHandler(final String inputPath, final String outputPath) {
        try {
            fs = new FileSystem(inputPath, outputPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Citirea hartii de joc
     */
    GameMap getGameMap() {
        try {
            int n = fs.nextInt();
            int m = fs.nextInt();
            String s;
            AreaTypes type;

            GameMap gameMap = new GameMap(n, m);

            for (int i = 0; i < n; i++) {
                s = fs.nextWord();
                for (int j = 0; j < m; j++) {
                    switch (s.charAt(j)) {
                        case 'V':
                            type = AreaTypes.volcanic;
                            break;
                        case 'L':
                            type = AreaTypes.land;
                            break;
                        case 'W':
                            type = AreaTypes.woods;
                            break;
                        case 'D':
                            type = AreaTypes.desert;
                            break;
                        default:
                            type = AreaTypes.land;
                            System.out.println("Land type error");
                    }

                    gameMap.changeAreaType(type, i, j);
                }
            }

            return gameMap;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /*
     * Citirea eroilor
     */
    ArrayList<Hero> getHeroes() {
        HeroFactory heroFactory = HeroFactory.getInstance();
        ArrayList<Hero> heroes = new ArrayList<>();

        int noHeroes;
        int x, y;
        char c;
        try {
            noHeroes = fs.nextInt();
            for (int i = 0; i < noHeroes; i++) {
                c = fs.nextWord().charAt(0);
                x = fs.nextInt();
                y = fs.nextInt();

                /*
                 * Generarea eroilor se face cu o clasa Factory
                 */
                heroes.add(heroFactory.getHero(c, x, y));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return heroes;
    }

    int getInt() {
        try {
            return fs.nextInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    String getWord() {
        try {
            return fs.nextWord();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    void printString(final String s) {
        try {
            fs.writeWord(s);
            fs.writeNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Functie ce inchide FileSystemul fs
     */
    void deconstruct() {
        try {
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void putLine() {
        try {
            fs.writeNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
