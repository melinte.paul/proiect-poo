package main;

import common.Pair;
import heroes.Hero;
import map.GameMap;

import java.util.ArrayList;

/*
 * Clasa ce se ocupa de rularea jocului
 */
final class Game {
    private IOHandler ioHandler;
    private GameMap gameMap;
    private ArrayList<Hero> heroes;
    private int noTurns;

    /*
     * Initializarea jocului
     */
    Game(final IOHandler ioHandler) {
        this.ioHandler = ioHandler;
        gameMap = ioHandler.getGameMap();
        heroes = ioHandler.getHeroes();
        noTurns = ioHandler.getInt();
    }

    /*
     * Fiecare tura este rulata in functia runTurn
     * La final se afiseaza rezultatele si se inchide
     * clasa de citire/scriere
     */
    void run() {
        for (int i = 0; i < noTurns; i++) {
            runTurn();
        }

        printResults();

        ioHandler.deconstruct();
    }

    private void printResults() {
        for (Hero hero : heroes) {
            ioHandler.printString(hero.toString());
        }
        ioHandler.putLine();
    }

    private void runTurn() {
        String moves = ioHandler.getWord();
        if (moves == null) {
            System.out.println("eroare miscari");
            return;
        }

        /*
         * Ordinea unei ture este aceasta:
         * Miscarea tuturor eroilor
         * Calcul damage-over-time
         * Calcul perechi de eroi ce se lupta
         * Calcul lupte
         * Calcul damage in fiecare tura
         * Calcul XP si level-up
         */

        for (int i = 0; i < heroes.size(); i++) {
            heroes.get(i).movement(moves.charAt(i));
        }

        for (Hero hero : heroes) {
            hero.calculatePreTurn();
        }

        ArrayList<Pair<Hero, Hero>> fights = getFights();

        for (Pair<Hero, Hero> pair : fights) {
            pair.getValue().acceptFight(pair.getKey(), gameMap.getAreaType(pair.getKey().getPos()));
            pair.getKey().acceptFight(pair.getValue(), gameMap.getAreaType(pair.getKey().getPos()));
        }

        for (Hero hero : heroes) {
            hero.calculateEndTurn();
        }

        for (Hero hero : heroes) {
            hero.calculateXP();
        }

    }

    /*
     * Generarea de perechi de eroi ce se lupta
     * Fiecare erou are o prioritate; facem acest
     * artificiu pentru a garanta ca un erou Wizard se bate al doilea
     */
    private ArrayList<Pair<Hero, Hero>> getFights() {
        ArrayList<Pair<Hero, Hero>> fights = new ArrayList<>();
        Hero hero1;
        Hero hero2;
        for (int i = 0; i < heroes.size(); i++) {
            for (int j = i + 1; j < heroes.size(); j++) {
                hero1 = heroes.get(i);
                hero2 = heroes.get(j);

                if (!hero1.isDead() && !hero2.isDead()) {
                    if (hero1.getPos().equals(hero2.getPos())) {
                        if (hero1.getPriority() <= hero2.getPriority()) {
                            fights.add(new Pair<>(hero1, hero2));
                        } else {
                            fights.add(new Pair<>(hero2, hero1));
                        }
                    }
                }

            }
        }
        return fights;
    }
}
